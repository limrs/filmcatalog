<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('/director', 'DirectorController', ['except' => [
	'destroy',
]]);

Route::resource('/actor', 'ActorController', ['except' => [
	'destroy',
]]);

Route::resource('/film', 'FilmController', ['except' => [
	'destroy',
]]);

Route::resource('/user', 'UserController');

//Auth::routes();
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/listActor', 'PublicController@listActor')->name('listActor');
Route::get('/showActor/{actor}', 'PublicController@showActor')->name('showActor');
Route::get('/listDirector', 'PublicController@listDirector')->name('listDirector');
Route::get('/showDirector/{director}', 'PublicController@showDirector')->name('showDirector');
Route::get('/listFilm', 'PublicController@listFilm')->name('listFilm');
Route::get('/showFilm/{film}', 'PublicController@showFilm')->name('showFilm');
