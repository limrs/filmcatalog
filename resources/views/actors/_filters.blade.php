<?php

use App\Common;

?>
<section class="filters">
	<div class="panel-body">
		{!! Form::open([
			'route' => ['actor.index'],
			'method' => 'get',
			'class' => 'form-inline'
		]) !!}
		
		<div class="form-group">
			{!! Form::label('actor-name', 'Name', [
				'class' => 'control-label',
			]) !!}
			{!! Form::text('name', null, [
				'id' => 'actor-name',
				'class' => 'form-control',
				'maxlength' => 100,
			]) !!}
		</div>
		
		<div class="form-group">
			{!! Form::label('actor-gender', 'Gender', [
				'class' => 'control-label',
			]) !!}
			{!! Form::select('gender', Common::$genders, null, [
				'class' => 'form-control',
				'placeholder' => '- All -',
			]) !!}
		</div>
		
		<div class="form-group">
			{!! Form::label('actor-born', 'Born', [
				'class' => 'control-label',
			]) !!}
			{!! Form::text('born', null, [
				'id' => 'datepicker',
				'class' => 'form-control',
				'maxlength' => 10,
			]) !!}
		</div>
		
		<div class="form-group">
			{!! Form::button('Filter', [
				'type' => 'submit',
				'class' => 'btn btn-primary',
			]) !!}
		</div>
		{!! Form::close() !!}
	</div>
</section>
