<?php

use App\Common;

?>
@extends('layouts.app')

@section('content')

	<!-- Bootstrap Boilerplate... -->

	<div class="panel-body">
	<a href="{{ route('actor.index') }}" class="btn btn-primary">Back to Actor List</a>
	<a href="{{ route('actor.edit', $actor->id) }}" class="btn btn-success">Update Information</a>
		<table class="table table-striped task-table">
			<!-- Table Headings -->
			<thead>
				<tr>
					<th>Attribute</th>
					<th>Value</th>
				</tr>
			</thead>
			<!-- Table Body -->
			<tbody>
				<tr>
					<td>Name</td>
					<td>{{ $actor->name }}</td>
				</tr>
				<tr>
					<td>Gender</td>
					<td>{{ Common::$genders[$actor->gender] }}</td>
				</tr>
				<tr>
					<td>Born</td>
					<td>{{ $actor->born }}</td>
				</tr>
				<tr>
					<td>Film(s)</td>
					<td>
						<ul>
							@foreach ($actor->films as $film)
								<li>
									<div>
										{!! link_to_route(
											'film.show',
											$title = $film->name,
											$parameters = [
												'id' => $film->id,
											]
										) !!}
									</div>
								</li>
							@endforeach
						</ul>
					</td>
				</tr>
				<tr>
					<td>Created</td>
					<td>{{ $actor->created_at }}</td>
				</tr>
				<tr>
					<td>Updated</td>
					<td>{{ $actor->updated_at }}</td>
				</tr>
			</tbody>
		</table>
	</div>

@endsection
