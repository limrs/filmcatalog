<?php

use App\Common;

?>
@extends('layouts.app')
@section('content')

<!-- Bootstrap Boilerplate... -->
<div class="panel-body">
<a href="{{ route('actor.create') }}" class="btn btn-success">Create Actor</a>
@include('actors._filters')
	@if (count($actors) > 0)
		<table class="table table-striped task-table">
			<!-- Table Headings -->
			<thead>
				<tr>
					<th>No.</th>
					<th>Name</th>
					<th>Gender</th>
					<th>Born</th>
					<th>Created</th>
					<th>Actions</th>
				</tr>
			</thead>
			
			<!-- Table Body -->
			<tbody>
				@foreach ($actors as $i => $actor)
				<tr>
					<td class="table-text">
						<div>{{ $i+1 }}</div>
					</td>
					<td class="table-text">
						<div>
							{!! link_to_route(
								'actor.show',
								$title = $actor->name,
								$parameters = [
									'id' => $actor->id,
								]
							) !!}
						</div>
					</td>
					<td class="table-text">
						<div>{{ Common::$genders[$actor->gender] }}</div>
					</td>
					<td class="table-text">
						<div>{{ $actor->born }}</div>
					</td>
					<td class="table-text">
						<div>{{ $actor->created_at }}</div>
					</td>
					<td class="table-text">
						<div>
						{!! link_to_route(
							'actor.edit',
							$title = 'Edit',
							$parameters = [
								'id' => $actor->id,
							]
						) !!}
						</div>
					</td>
				</tr>
				@endforeach
				{{ $actors->links() }}
			</tbody>
		</table>
	@else
	<div>
		No records found
	</div>
	@endif
</div>
@endsection
