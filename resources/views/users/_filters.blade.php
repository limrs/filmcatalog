<?php

use App\Common;

?>
<section class="filters">
	<div class="panel-body">
		{!! Form::open([
			'route' => ['user.index'],
			'method' => 'get',
			'class' => 'form-inline'
		]) !!}
		
		<div class="form-group">
			{!! Form::label('user-name', 'Name', [
				'class' => 'control-label',
			]) !!}
			{!! Form::text('name', null, [
				'id' => 'user-name',
				'class' => 'form-control',
				'maxlength' => 255,
			]) !!}
		</div>
		
		<div class="form-group">
			{!! Form::label('user-email', 'Email', [
				'class' => 'control-label',
			]) !!}
			{!! Form::text('born', null, [
				'id' => 'user-email',
				'class' => 'form-control',
				'maxlength' => 255,
			]) !!}
		</div>
		
		<div class="form-group">
			{!! Form::button('Filter', [
				'type' => 'submit',
				'class' => 'btn btn-primary',
			]) !!}
		</div>
		{!! Form::close() !!}
	</div>
</section>
