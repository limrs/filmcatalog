<?php

use App\Common;
?>
@extends('layouts.app')
@section('content')
<!-- Bootstrap Boilerplate... -->
<div class="panel-body">
	<a href="{{ route('user.create') }}" class="btn btn-success">Create Staff Account</a>
	@include('users._filters')
	@if (count($users) > 0)
		<table class="table table-striped task-table">
			<!-- Table Headings -->
			<thead>
				<tr>
					<th>No.</th>
					<th>Name</th>
					<th>Email</th>
					<th>Role</th>
					<th>Created</th>
					<th>Actions</th>
				</tr>
			</thead>
			
			<!-- Table Body -->
			<tbody>
				@foreach ($users as $i => $user)
				<tr>
					<td class="table-text">
						<div>{{ $i+1 }}</div>
					</td>
					<td class="table-text">
						<div>
							{!! link_to_route(
								'user.show',
								$title = $user->name,
								$parameters = [
									'id' => $user->id,
								]
							) !!}
						</div>
					</td>
					<td class="table-text">
						<div>{{ $user->email }}</div>
					</td>
					<td class="table-text">
						@if ($user->isAn('admin'))
						<div>{{ 'Admin' }}</div>
						@else
						<div>{{ 'Staff' }}</div>
						@endif
					</td>
					<td class="table-text">
						<div>{{ $user->created_at }}</div>
					</td>
					<td class="table-text">
						<div>
						{!! link_to_route(
							'user.edit',
							$title = 'Edit',
							$parameters = [
								'id' => $user->id,
							]
						) !!}
						</div>
						<form onsubmit="return confirm('Do you really want to delete?');" action="{{ route('user.destroy',[$user->id]) }}" method="POST" >
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="DELETE" />
						<button type="submit">Delete</i></button>
						</form>
					</td>
				</tr>
				@endforeach
				{{ $users->links() }}
			</tbody>
		</table>
	@else
	<div>
		No records found
	</div>
	@endif
</div>
@endsection
