<?php

use App\Common;

?>
@extends('layouts.app')

@section('content')

	<!-- Bootstrap Boilerplate... -->

	<div class="panel-body">
	<a href="{{ route('user.index') }}" class="btn btn-primary">Back to Staff List</a>
	<a href="{{ route('user.edit', $user->id) }}" class="btn btn-success">Update Information</a>
		<table class="table table-striped task-table">
			<!-- Table Headings -->
			<thead>
				<tr>
					<th>Attribute</th>
					<th>Value</th>
				</tr>
			</thead>
			<!-- Table Body -->
			<tbody>
				<tr>
					<td>Name</td>
					<td>{{ $user->name }}</td>
				</tr>
				<tr>
					<td>Email</td>
					<td>{{ $user->email }}</td>
				</tr>
				<tr>
					<td>Role</td>
					@if ($user->isAn('admin'))
						<td>{{ 'Admin' }}</td>
					@else
						<td>{{ 'Staff' }}</td>
					@endif
				</tr>
				<tr>
					<td>Created</td>
					<td>{{ $user->created_at }}</td>
				</tr>
				<tr>
					<td>Updated</td>
					<td>{{ $user->updated_at }}</td>
				</tr>
			</tbody>
		</table>
	</div>

@endsection
