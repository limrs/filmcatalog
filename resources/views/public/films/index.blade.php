<?php

use App\Common;

?>
@extends('layouts.app')
@section('content')

<!-- Bootstrap Boilerplate... -->
<div class="panel-body">
@include('public/films._filters')
	@if (count($films) > 0)
		<table class="table table-striped task-table">
			<!-- Table Headings -->
			<thead>
				<tr>
					<th>No.</th>
					<th>Name</th>
					<th>Release Date</th>
					<th>Country</th>
					<th>Runtime</th>
					<th>Genre</th>
					<th>Created</th>
				</tr>
			</thead>
			
			<!-- Table Body -->
			<tbody>
				@foreach ($films as $i => $film)
				<tr>
					<td class="table-text">
						<div>{{ $i+1 }}</div>
					</td>
					<td class="table-text">
						<div>
							{!! link_to_route(
								'showFilm',
								$title = $film->name,
								$parameters = [
									'id' => $film->id,
								]
							) !!}
						</div>
					</td>
					<td class="table-text">
						<div>{{ $film->release_date }}</div>
					</td>
					<td class="table-text">
						<div>{{ Common::$countries[$film->country] }}</div>
					</td>
					<td class="table-text">
						<div>{{ $film->runtime }}</div>
					</td>
					<td class="table-text">
						<div>{{ Common::$genres[$film->genre] }}</div>
					</td>
					<td class="table-text">
						<div>{{ $film->created_at }}</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		{{ $films->links() }}
	@else
	<div>
		No records found
	</div>
	@endif
</div>
@endsection
