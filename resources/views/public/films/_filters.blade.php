<?php

use App\Common;

?>
<section class="filters">
	<div class="panel-body">
		{!! Form::open([
			'route' => ['listFilm'],
			'method' => 'get',
			'class' => 'form-inline'
		]) !!}
		
		<div class="form-group">
			{!! Form::label('film-name', 'Name', [
				'class' => 'control-label',
			]) !!}
			{!! Form::text('name', null, [
				'id' => 'film-name',
				'class' => 'form-control',
				'maxlength' => 100,
			]) !!}
		</div>

		<div class="form-group">
			{!! Form::label('film-release-date', 'Release Date', [
				'class' => 'control-label',
			]) !!}
			{!! Form::text('release_date', null, [
				'id' => 'datepicker',
				'class' => 'form-control',
				'maxlength' => 10,
			]) !!}
		</div>
		
		<div class="form-group">
			{!! Form::label('film-country', 'Country', [
				'class' => 'control-label',
			]) !!}
			{!! Form::select('country', Common::$countries, null, [
				'class' => 'form-control',
				'placeholder' => '- All -',
			]) !!}
		</div>
		
		<div class="form-group">
			{!! Form::label('film-genre', 'Genre', [
				'class' => 'control-label',
			]) !!}
			{!! Form::select('genre', Common::$genres, null, [
				'class' => 'form-control',
				'placeholder' => '- All -',
			]) !!}
		</div>
		
		<div class="form-group">
			{!! Form::button('Filter', [
				'type' => 'submit',
				'class' => 'btn btn-primary',
			]) !!}
		</div>
		{!! Form::close() !!}
	</div>
</section>
