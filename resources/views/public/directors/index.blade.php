<?php

use App\Common;

?>
@extends('layouts.app')
@section('content')
<!-- Bootstrap Boilerplate... -->
<div class="panel-body">
@include('public.directors._filters')
	@if (count($directors) > 0)
		<table class="table table-striped task-table">
			<!-- Table Headings -->
			<thead>
				<tr>
					<th>No.</th>
					<th>Name</th>
					<th>Gender</th>
					<th>Born</th>
					<th>Created</th>
				</tr>
			</thead>
			
			<!-- Table Body -->
			<tbody>
				@foreach ($directors as $i => $director)
				<tr>
					<td class="table-text">
						<div>{{ $i+1 }}</div>
					</td>
					<td class="table-text">
						<div>
							{!! link_to_route(
								'showDirector',
								$title = $director->name,
								$parameters = [
									'id' => $director->id,
								]
							) !!}
						</div>
					</td>
					<td class="table-text">
						<div>{{ Common::$genders[$director->gender] }}</div>
					</td>
					<td class="table-text">
						<div>{{ $director->born }}</div>
					</td>
					<td class="table-text">
						<div>{{ $director->created_at }}</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		{{ $directors->links() }}
	@else
	<div>
		No records found
	</div>
	@endif
</div>
@endsection
