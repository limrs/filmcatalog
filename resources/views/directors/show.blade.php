<?php

use App\Common;

?>
@extends('layouts.app')

@section('content')

	<!-- Bootstrap Boilerplate... -->

	<div class="panel-body">
	<a href="{{ route('director.index') }}" class="btn btn-primary">Back to Director List</a>
	<a href="{{ route('director.edit', $director->id) }}" class="btn btn-success">Update Information</a>
		<table class="table table-striped task-table">
			<!-- Table Headings -->
			<thead>
				<tr>
					<th>Attribute</th>
					<th>Value</th>
				</tr>
			</thead>
			<!-- Table Body -->
			<tbody>
				<tr>
					<td>Name</td>
					<td>{{ $director->name }}</td>
				</tr>
				<tr>
					<td>Gender</td>
					<td>{{ Common::$genders[$director->gender] }}</td>
				</tr>
				<tr>
					<td>Born</td>
					<td>{{ $director->born }}</td>
				</tr>
				<tr>
					<td>Film(s)</td>
					<td>
						<ul>
							@foreach ($director->films as $film)
								<li>
									<div>
										{!! link_to_route(
											'film.show',
											$title = $film->name,
											$parameters = [
												'id' => $film->id,
											]
										) !!}
									</div>
								</li>
							@endforeach
						</ul>
					</td>
				</tr>
				<tr>
					<td>Created</td>
					<td>{{ $director->created_at }}</td>
				</tr>
				<tr>
					<td>Updated</td>
					<td>{{ $director->updated_at }}</td>
				</tr>
			</tbody>
		</table>
	</div>

@endsection
