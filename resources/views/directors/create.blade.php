<?php

use App\Common;

?>
@extends('layouts.app')

@section('content')

	@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
 
	<!-- Bootstrap Boilerplate... -->
	
	<div class="panel-body">
		<!-- New Director Form -->
		{!! Form::model($director, [
			'route' => ['director.store'],
			'class' => 'form-horizontal'
		]) !!}
		
		<!-- Name -->
		<div class="form-group row">
			{!! Form::label('director-name', 'Name', [
				'class' => 'control-label col-sm-3',
			]) !!}
			<div class="col-sm-9">
				{!! Form::text('name', null, [
					'id' => 'director-name',
					'class' => 'form-control',
					'maxlength' => 100,
				]) !!}
			</div>
		</div>
		
		<!-- Gender -->
		<div class="form-group row">
			{!! Form::label('gender', 'Gender', [
				'class' => 'control-label col-sm-3',
			]) !!}
			<div class="col-sm-9">
				@foreach(Common::$genders as $key => $val)
					{!! Form::radio('gender', $key) !!} {{$val}}
				@endforeach
			</div>
		</div>
		
		<!-- Born -->
		<div class="form-group row">
			{!! Form::label('director-born', 'Born', [
				'class' => 'control-label col-sm-3',
			]) !!}
			<div class="col-sm-9">
				{!! Form::text('born', null, [
					'id' => 'datepicker',
					'class' => 'form-control',
				]) !!}
			</div>
		</div>
		
		<!-- Submit Button -->
		<div class="form-group row">
			<div class="col-sm-offset-3 col-sm-6">
			{!! Form::button('Submit', [
				'type' => 'submit',
				'class' => 'btn btn-primary',
			]) !!}
			<a href="{{ route('director.index') }}" class="btn btn-warning">Back</a>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
@endsection