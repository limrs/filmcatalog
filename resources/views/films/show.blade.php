<?php

use App\Common;

?>
@extends('layouts.app')

@section('content')

	<!-- Bootstrap Boilerplate... -->

	<div class="panel-body">
	<a href="{{ route('film.index') }}" class="btn btn-primary">Back to Film List</a>
	<a href="{{ route('film.edit', $film->id) }}" class="btn btn-success">Update Information</a>
		<table class="table table-striped task-table">
			<!-- Table Headings -->
			<thead>
				<tr>
					<th>Attribute</th>
					<th>Value</th>
				</tr>
			</thead>
			<!-- Table Body -->
			<tbody>
				<tr>
					<td>Name</td>
					<td>{{ $film->name }}</td>
				</tr>
				<tr>
					<td>Release Date</td>
					<td>{{ $film->release_date }}</td>
				</tr>
				<tr>
					<td>Country</td>
					<td>{{ Common::$countries[$film->country] }}</td>
				</tr>
				<tr>
					<td>Runtime</td>
					<td>{{ $film->runtime }}</td>
				</tr>
				<tr>
					<td>Genre</td>
					<td>{{ Common::$genres[$film->genre] }}</td>
				</tr>
				<tr>
					<td>Synopsis</td>
					<td>{{ $film->synopsis }}</td>
				</tr>
				<tr>
					<td>Director</td>
					<td>
						<div>
							{!! link_to_route(
								'director.show',
								$title = $film->director->name,
								$parameters = [
									'id' => $film->director->id,
								]
							) !!}
						</div>
					</td>
				</tr>
				<tr>
					<td>Starring(s)</td>
					<td>
						<ul>
							@foreach ($film->actors as $actor)
								<li>
								<div>
									{!! link_to_route(
										'actor.show',
										$title = $actor->name,
										$parameters = [
											'id' => $actor->id,
										]
									) !!}
								</div>
								</li>
							@endforeach
						</ul>
					</td>
				</tr>
				<tr>
					<td>Created</td>
					<td>{{ $film->created_at }}</td>
				</tr>
				<tr>
					<td>Updated</td>
					<td>{{ $film->updated_at }}</td>
				</tr>
			</tbody>
		</table>
	</div>

@endsection
