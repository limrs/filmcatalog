<?php

use App\Common;

?>
@extends('layouts.app')
@section('content')

<!-- Bootstrap Boilerplate... -->
<div class="panel-body">
<a href="{{ route('film.create') }}" class="btn btn-success">Create Film</a>
@include('films._filters')
	@if (count($films) > 0)
		<table class="table table-striped task-table">
			<!-- Table Headings -->
			<thead>
				<tr>
					<th>No.</th>
					<th>Name</th>
					<th>Release Date</th>
					<th>Country</th>
					<th>Runtime</th>
					<th>Genre</th>
					<th>Created</th>
					<th>Actions</th>
				</tr>
			</thead>
			
			<!-- Table Body -->
			<tbody>
				@foreach ($films as $i => $film)
				<tr>
					<td class="table-text">
						<div>{{ $i+1 }}</div>
					</td>
					<td class="table-text">
						<div>
							{!! link_to_route(
								'film.show',
								$title = $film->name,
								$parameters = [
									'id' => $film->id,
								]
							) !!}
						</div>
					</td>
					<td class="table-text">
						<div>{{ $film->release_date }}</div>
					</td>
					<td class="table-text">
						<div>{{ Common::$countries[$film->country] }}</div>
					</td>
					<td class="table-text">
						<div>{{ $film->runtime }}</div>
					</td>
					<td class="table-text">
						<div>{{ Common::$genres[$film->genre] }}</div>
					</td>
					<td class="table-text">
						<div>{{ $film->created_at }}</div>
					</td>
					<td class="table-text">
						<div>
						{!! link_to_route(
							'film.edit',
							$title = 'Edit',
							$parameters = [
								'id' => $film->id,
							]
						) !!}
						</div>
					</td>
				</tr>
				@endforeach
				{{ $films->links() }}
			</tbody>
		</table>
	@else
	<div>
		No records found
	</div>
	@endif
</div>
@endsection
