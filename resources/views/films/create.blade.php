<?php

use App\Common;
use App\Director;
use App\Actor;

?>
@extends('layouts.app')

@section('content')

	@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
	
	<!-- Bootstrap Boilerplate... -->
	
	<div class="panel-body">
		<!-- New Film Form -->
		{!! Form::model($film, [
			'route' => ['film.store'],
			'class' => 'form-horizontal'
		]) !!}
		
		<!-- Name -->
		<div class="form-group row">
			{!! Form::label('film-name', 'Name', [
				'class' => 'control-label col-sm-3',
			]) !!}
			<div class="col-sm-9">
				{!! Form::text('name', null, [
					'id' => 'film-name',
					'class' => 'form-control',
					'maxlength' => 100,
				]) !!}
			</div>
		</div>
		
		<!-- Release Date -->
		<div class="form-group row">
			{!! Form::label('film-release-date', 'Release Date', [
				'class' => 'control-label col-sm-3',
			]) !!}
			<div class="col-sm-9">
				{!! Form::text('release_date', null, [
					'id' => 'datepicker',
					'class' => 'form-control',
				]) !!}
			</div>
		</div>
		
	    <!-- Country -->
	    <div class="form-group row">
			{!! Form::label('film-country', 'Country', [
				'class' => 'control-label col-sm-3',
			]) !!}
			<div class="col-sm-9">
				{!! Form::select('country', Common::$countries, null, [
					'class' => 'form-control',
					'placeholder' => '- Select Country -',
				]) !!}
			</div>
	    </div>
	   
		<!-- Run Time -->
		<div class="form-group row">
			{!! Form::label('film-runtime', 'Runtime', [
				'class' => 'control-label col-sm-3',
			]) !!}
			<div class="col-sm-9">
				{!! Form::text('runtime', null, [
					'id' => 'timepicker',
					'class' => 'form-control',
					'maxlength' => 100,
				]) !!}
			</div>
		</div>
		
	    <!-- Genre -->
	    <div class="form-group row">
			{!! Form::label('film-genre', 'Genre', [
				'class' => 'control-label col-sm-3',
			]) !!}
			<div class="col-sm-9">
				{!! Form::select('genre', Common::$genres, null, [
					'class' => 'form-control',
					'placeholder' => '- Select Genre -',
				]) !!}
			</div>
	    </div>
		
		<!-- Synopsis -->
		<div class="form-group row">
			{!! Form::label('film-synopsis', 'Synopsis', [
				'class' => 'control-label col-sm-3',
			]) !!}
			<div class="col-sm-9">
				{!! Form::textarea('synopsis', null, [
					'id' => 'film-synopsis',
					'class' => 'form-control',
				]) !!}
			</div>
		</div>

		<!-- Director -->
		<div class="form-group row">
			{!! Form::label('film-director', 'Director', [
				'class' => 'control-label col-sm-3',
			]) !!}
			<div class="col-sm-9">
				{!! Form::select('director_id', 
					Director::pluck('name', 'id'),
					null, [
						'class' => 'form-control',
						'placeholder' => '- Select Director -',
				]) !!}
			</div>
		</div>
		
        <!-- Actor -->
		<div class="form-group row">
			{!! Form::label('film-actor', 'Starring(s)', [
				'class' => 'control-label col-sm-3',
			]) !!}
			<div class="col-sm-9">
				{!! Form::select('actors_id[]', 
					Actor::pluck('name', 'id'),
					null, [
						'class' => 'form-control select2',
						'multiple' => 'multiple',
						'data-placeholder' => '- Select Starring(s) -',
				]) !!}
			</div>
		</div>
		
		<!-- Submit Button -->
		<div class="form-group row">
			<div class="col-sm-offset-3 col-sm-6">
			{!! Form::button('Submit', [
				'type' => 'submit',
				'class' => 'btn btn-primary',
			]) !!}
			<a href="{{ route('film.index') }}" class="btn btn-warning">Back</a>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
@endsection