<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;

class InitRolesAndPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Define roles
		$admin = Bouncer::role()->create([
			'name' => 'admin',
			'title' => 'Administrator',
		]);
		
		$staff = Bouncer::role()->create([
			'name' => 'staff',
			'title' => 'Staff',
		]);
		
		// Define abilities
		$viewFilm = Bouncer::ability()->create([
			'name' => 'view-film',
			'title' => 'View Film',
		]);
		
		$createFilm = Bouncer::ability()->create([
			'name' => 'create-film',
			'title' => 'Create Film',
		]);
		
		$manageFilm = Bouncer::ability()->create([
			'name' => 'manage-film',
			'title' => 'Manage Film',
		]);
		
		$viewActor = Bouncer::ability()->create([
			'name' => 'view-actor',
			'title' => 'View Actor',
		]);
		
		$createActor = Bouncer::ability()->create([
			'name' => 'create-actor',
			'title' => 'Create Actor',
		]);
		
		$manageActor = Bouncer::ability()->create([
			'name' => 'manage-actor',
			'title' => 'Manage Actor',
		]);
		
		$viewDirector = Bouncer::ability()->create([
			'name' => 'view-director',
			'title' => 'View Director',
		]);
		
		$createDirector = Bouncer::ability()->create([
			'name' => 'create-director',
			'title' => 'Create Director',
		]);
		
		$manageDirector = Bouncer::ability()->create([
			'name' => 'manage-director',
			'title' => 'Manage Director',
		]);
		
				$viewDirector = Bouncer::ability()->create([
			'name' => 'view-director',
			'title' => 'View Director',
		]);
		
		$viewUser = Bouncer::ability()->create([
			'name' => 'view-user',
			'title' => 'View User',
		]);
		
		$createUser = Bouncer::ability()->create([
			'name' => 'create-user',
			'title' => 'Create User',
		]);
		
		$manageUser = Bouncer::ability()->create([
			'name' => 'manage-user',
			'title' => 'Manage User',
		]);
		
		// Assign abilities to roles
		Bouncer::allow($staff)->to($viewFilm);
		Bouncer::allow($staff)->to($createFilm);
		Bouncer::allow($staff)->to($manageFilm);
		Bouncer::allow($staff)->to($viewActor);
		Bouncer::allow($staff)->to($createActor);
		Bouncer::allow($staff)->to($manageActor);
		Bouncer::allow($staff)->to($viewDirector);
		Bouncer::allow($staff)->to($createDirector);
		Bouncer::allow($staff)->to($manageDirector);
		
		Bouncer::allow($admin)->to($viewFilm);
		Bouncer::allow($admin)->to($createFilm);
		Bouncer::allow($admin)->to($manageFilm);
		Bouncer::allow($admin)->to($viewActor);
		Bouncer::allow($admin)->to($createActor);
		Bouncer::allow($admin)->to($manageActor);
		Bouncer::allow($admin)->to($viewDirector);
		Bouncer::allow($admin)->to($createDirector);
		Bouncer::allow($admin)->to($manageDirector);
		Bouncer::allow($admin)->to($viewUser);
		Bouncer::allow($admin)->to($createUser);
		Bouncer::allow($admin)->to($manageUser);
		
		// Make the first user an admin
		$user = User::find(1);
		Bouncer::assign('admin')->to($user);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
