<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertActorsData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('actors')->insert([
          ['name' => 'Kim Min-hee', 'gender' => 'F', 'born' => '1982-01-01', "created_at" =>  \Carbon\Carbon::now(), "updated_at" =>  \Carbon\Carbon::now()],
          ['name' => 'Kim Tae-ri', 'gender' => 'F', 'born' => '1990-04-24', "created_at" =>  \Carbon\Carbon::now(), "updated_at" =>  \Carbon\Carbon::now()],
		  ['name' => 'Ha Jung-woo', 'gender' => 'M', 'born' => '1978-03-11', "created_at" =>  \Carbon\Carbon::now(), "updated_at" =>  \Carbon\Carbon::now()],
		  ['name' => 'Jo Won-joon', 'gender' => 'M', 'born' => '1976-04-02', "created_at" =>  \Carbon\Carbon::now(), "updated_at" =>  \Carbon\Carbon::now()],
		  ['name' => 'Aamir Khan', 'gender' => 'M', 'born' => '1965-03-14', "created_at" =>  \Carbon\Carbon::now(), "updated_at" =>  \Carbon\Carbon::now()],
		  ['name' => 'R. Madhavan', 'gender' => 'M', 'born' => '1970-06-01', "created_at" =>  \Carbon\Carbon::now(), "updated_at" =>  \Carbon\Carbon::now()],
		  ['name' => 'Sharman Joshi', 'gender' => 'M', 'born' => '1979-04-28', "created_at" =>  \Carbon\Carbon::now(), "updated_at" =>  \Carbon\Carbon::now()],
		  ['name' => 'Kareena Kapoor', 'gender' => 'F', 'born' => '1980-09-21', "created_at" =>  \Carbon\Carbon::now(), "updated_at" =>  \Carbon\Carbon::now()],
		  ['name' => 'Voman Irani', 'gender' => 'M', 'born' => '1959-12-02', "created_at" =>  \Carbon\Carbon::now(), "updated_at" =>  \Carbon\Carbon::now()],
		  ['name' => 'Omi Vaida', 'gender' => 'M', 'born' => '1982-01-10', "created_at" =>  \Carbon\Carbon::now(), "updated_at" =>  \Carbon\Carbon::now()],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
