<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertFilmsData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::table('films')->insert([
			['name' => 'The Handmaiden', 'release_date' => '2016-05-14', 'country' => 'KR', 'runtime' => '02:27:00', 'genre' => '17', 'synopsis' => 'With help from an orphaned pickpocket (Kim Tae-ri), a Korean con man (Ha Jung-woo) devises an elaborate plot to seduce and bilk a Japanese woman (Kim Min-hee) out of her inheritance.', 'director_id' => 1, "created_at" =>  \Carbon\Carbon::now(), "updated_at" =>  \Carbon\Carbon::now()],
			['name' => '3 Idiots', 'release_date' => '2009-12-25', 'country' => 'IN', 'runtime' => '02:51:00', 'genre' => '04', 'synopsis' => 'In college, Farhan and Raju form a great bond with Rancho due to his positive and refreshing outlook to life. Years later, a bet gives them a chance to look for their long-lost friend whose existence seems rather elusive.', 'director_id' => 2, "created_at" =>  \Carbon\Carbon::now(), "updated_at" =>  \Carbon\Carbon::now()],
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
