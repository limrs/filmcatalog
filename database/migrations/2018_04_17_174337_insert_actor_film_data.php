<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertActorFilmData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::table('actor_film')->insert([
			['actor_id' => 1, 'film_id' => 1],
			['actor_id' => 2, 'film_id' => 1],
			['actor_id' => 3, 'film_id' => 1],
			['actor_id' => 4, 'film_id' => 1],
			['actor_id' => 5, 'film_id' => 2],
			['actor_id' => 6, 'film_id' => 2],
			['actor_id' => 7, 'film_id' => 2],
			['actor_id' => 8, 'film_id' => 2],
			['actor_id' => 9, 'film_id' => 2],
			['actor_id' => 10, 'film_id' => 2],
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
