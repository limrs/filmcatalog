<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDirectorsData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::table('directors')->insert([
			['name' => 'Park Chan-wook', 'gender' => 'M', 'born' => '1963-08-23', "created_at" =>  \Carbon\Carbon::now(), "updated_at" =>  \Carbon\Carbon::now()],
			['name' => 'Rajkumar Hirani', 'gender' => 'M', 'born' => '1962-11-20', "created_at" =>  \Carbon\Carbon::now(), "updated_at" =>  \Carbon\Carbon::now()],
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
