<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            $table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('name', 255)->index();
			$table->date('release_date');
			$table->char('country', 2)->index();
			$table->time('runtime');
			$table->char('genre', 2)->index();
			$table->text('synopsis');
			$table->unsignedInteger('director_id');
			$table->foreign('director_id')->references('id')->on('directors');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}
