<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Film;

class FilmController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	
    /**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$this->authorize('create', Film::class);
		
		$film = new Film();
		
		return view('films.create', [
			'film' => $film,
		]);
	}
	
    /**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->authorize('create', Film::class);
		
		$request->validate([
			'name' => 'required|max:100|unique:films',
			'release_date' => [
				'required',
				'regex:/^([0-1]{1})([0-9]{1})\/([0-3]{1})([0-9]{1})\/([0-9]{4})$/',
			],
			'country' => 'required',
			'runtime' => [
				'required',
				'regex:/^([0-2]{0,1})([0-9]{1})\:([0-5]{1})([0-9]{1})\:([0-5]{1})([0-9]{1})$/',
			],
			'genre' => 'required',
			'synopsis' => 'required',
			'director_id' => 'required',
			'actors_id' => 'required',
		]);
		
		$film = new Film();
		$film->fill($request->all());
		$film->release_date = date('Y-m-d',strtotime($film->release_date));
		if ($film->save())
		{
			$film->actors()->sync($request->actors_id,false);
		}
		
		return redirect()->route('film.index');
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$this->authorize('view', Film::class);
		
		$films = Film::orderBy('name', 'asc')
		->when($request->query('name'), function($query) use ($request) {
			return $query->where('name', 'like', '%'.$request->query('name').'%');
		})
		->when($request->query('release_date'), function($query) use ($request) {
			return $query->where('release_date', 'like', '%'.date('Y-m-d',strtotime($request->query('release_date'))).'%');
		})
		->when($request->query('country'), function($query) use ($request) {
			return $query->where('country', $request->query('country'));
		})
		->when($request->query('genre'), function($query) use ($request) {
			return $query->where('genre', $request->query('genre'));
		})
		->paginate(10);
		
		return view('films.index', [
			'films' => $films
		]);
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$this->authorize('view', Film::class);
		
		$film = Film::find($id);
		if (!$film) throw new ModelNotFoundException;
		
		return view('films.show', [
			'film' => $film
		]);
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$film = Film::find($id);
		if(!$film) throw new ModelNotFoundException;
		$this->authorize('manage', $film);
		return view('films.edit', [
			'film' => $film
		]);
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param int $id
	 *
	 * return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$request->validate([
			'name' => 'required|max:100|unique:films,name,'.$id,
			'release_date' => [
				'required',
				'regex:/^([0-1]{1})([0-9]{1})\/([0-3]{1})([0-9]{1})\/([0-9]{4})$/',
			],
			'country' => 'required',
			'runtime' => [
				'required',
				'regex:/^([0-2]{0,1})([0-9]{1})\:([0-5]{1})([0-9]{1})\:([0-5]{1})([0-9]{1})$/',
			],
			'genre' => 'required',
			'synopsis' => 'required',
			'director_id' => 'required',
			'actors_id' => 'required',
		]);
		
		$film = Film::find($id);
		if(!$film) throw new ModelNotFoundException;
		
		$this->authorize('manage', $film);
		
		$film->fill($request->all());
		$film->release_date = date('Y-m-d',strtotime($film->release_date));
		if ($film->save())
		{
			$film->actors()->detach();
			$film->actors()->sync($request->actors_id,false);
		}
		
		return redirect()->route('film.index');
	}
}
