<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Actor;
use App\Director;
use App\Film;
class PublicController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function listActor(Request $request)
    {
        $actors = Actor::orderBy('name', 'asc')
		->when($request->query('name'), function($query) use ($request) {
			return $query->where('name', 'like', '%'.$request->query('name').'%');
		})
		->when($request->query('gender'), function($query) use ($request) {
			return $query->where('gender', $request->query('gender'));
		})
		->when($request->query('born'), function($query) use ($request) {
			return $query->where('born', 'like', '%'.date('Y-m-d',strtotime($request->query('born'))).'%');
		})
		->paginate(10);
		
		return view('public/actors.index', [
			'actors' => $actors
		]);
    }
	
	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function showActor($id)
	{	
		$actor = Actor::find($id);
		if (!$actor) throw new ModelNotFoundException;
		
		return view('public/actors.show', [
			'actor' => $actor
		]);
	}
	
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function listDirector(Request $request)
    {
        $directors = Director::orderBy('name', 'asc')
		->when($request->query('name'), function($query) use ($request) {
			return $query->where('name', 'like', '%'.$request->query('name').'%');
		})
		->when($request->query('gender'), function($query) use ($request) {
			return $query->where('gender', $request->query('gender'));
		})
		->when($request->query('born'), function($query) use ($request) {
			return $query->where('born', 'like', '%'.date('Y-m-d',strtotime($request->query('born'))).'%');
		})
		->paginate(10);
		
		return view('public/directors.index', [
			'directors' => $directors
		]);
    }
	
	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function showDirector($id)
	{	
		$director = Director::find($id);
		if (!$director) throw new ModelNotFoundException;
		
		return view('public/directors.show', [
			'director' => $director
		]);
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function listFilm(Request $request)
	{
		$films = Film::orderBy('name', 'asc')
		->when($request->query('name'), function($query) use ($request) {
			return $query->where('name', 'like', '%'.$request->query('name').'%');
		})
		->when($request->query('release_date'), function($query) use ($request) {
			return $query->where('release_date', 'like', '%'.date('Y-m-d',strtotime($request->query('release_date'))).'%');
		})
		->when($request->query('country'), function($query) use ($request) {
			return $query->where('country', $request->query('country'));
		})
		->when($request->query('genre'), function($query) use ($request) {
			return $query->where('genre', $request->query('genre'));
		})
		->paginate(10);
		
		return view('public/films.index', [
			'films' => $films
		]);
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function showFilm($id)
	{
		$film = Film::find($id);
		if (!$film) throw new ModelNotFoundException;
		
		return view('public/films.show', [
			'film' => $film
		]);
	}
}
