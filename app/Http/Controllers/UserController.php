<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Bouncer;
class UserController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	
    /**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$this->authorize('create', User::class);
		
		$user = new User();
		
		return view('users.create', [
			'user' => $user,
		]);
	}
	
    /**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$request->validate([
			'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
		]);
		$this->authorize('create', User::class);
		$user = new User();
		$request->password = bcrypt($request->password);
		$user->fill($request->all());
		$user->password = bcrypt($user->password);
		if ($user->save())
		{
			Bouncer::assign($request->role)->to($user);
		}
		
		return redirect()->route('user.index');
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$this->authorize('view', User::class);
		
		$users = User::orderBy('name', 'asc')
		->when($request->query('name'), function($query) use ($request) {
			return $query->where('name', 'like', '%'.$request->query('name').'%');
		})
		->when($request->query('email'), function($query) use ($request) {
			return $query->where('email', 'like', '%'.$request->query('email').'%');
		})
		->paginate(10);
		
		return view('users.index', [
			'users' => $users
		]);
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$this->authorize('view', User::class);
		
		$user = User::find($id);
		if (!$user) throw new ModelNotFoundException;
		
		return view('users.show', [
			'user' => $user
		]);
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$user = User::find($id);
		if(!$user) throw new ModelNotFoundException;
		$this->authorize('manage', $user);
		return view('users.edit', [
			'user' => $user
		]);
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param int $id
	 *
	 * return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$request->validate([
			'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$id,
		]);
		
		$user = User::find($id);
		if(!$user) throw new ModelNotFoundException;
		
		$this->authorize('manage', $user);
		
		$user->fill($request->all());
		
		$user->save();
		if ($user->isAn('admin') && $request->role == 'staff') {
			Bouncer::retract('admin')->from($user);
			Bouncer::assign($request->role)->to($user);
		} else if ($user->isNotAn('admin') && $request->role == 'admin') {
			Bouncer::retract('staff')->from($user);
			Bouncer::assign($request->role)->to($user);
		}
		return redirect()->route('user.index');
	}
	
	/**
	 * Remove the specified resource from storage
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		User::destroy($id);
		return redirect()->route('user.index');
	}
}
