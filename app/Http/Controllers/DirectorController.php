<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Director;

class DirectorController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	
    /**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$this->authorize('create', Director::class);
		
		$director = new Director();
		
		return view('directors.create', [
			'director' => $director,
		]);
	}
	
    /**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->authorize('create', Director::class);
		
		$request->validate([
			'name' => 'required|max:100',
			'gender' => 'required',
			'born' => [
				'required',
				'regex:/^([0-1]{1})([0-9]{1})\/([0-3]{1})([0-9]{1})\/([0-9]{4})$/',
			],
		]);
		
		$director = new Director();
		$director->fill($request->all());
		$director->born = date('Y-m-d',strtotime($director->born));
		$director->save();
		var_dump ($director->born);
		return redirect()->route('director.index');
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$this->authorize('view', Director::class);
		
		$directors = Director::orderBy('name', 'asc')
		->when($request->query('name'), function($query) use ($request) {
			return $query->where('name', 'like', '%'.$request->query('name').'%');
		})
		->when($request->query('gender'), function($query) use ($request) {
			return $query->where('gender', $request->query('gender'));
		})
		->when($request->query('born'), function($query) use ($request) {
			return $query->where('born', 'like', '%'.date('Y-m-d',strtotime($request->query('born'))).'%');
		})
		->paginate(10);
		return view('directors.index', [
			'directors' => $directors
		]);
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$this->authorize('view', Director::class);
		
		$director = Director::find($id);
		if (!$director) throw new ModelNotFoundException;
		
		return view('directors.show', [
			'director' => $director
		]);
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$director = Director::find($id);
		if(!$director) throw new ModelNotFoundException;
		
		$this->authorize('manage', $director);
		
		return view('directors.edit', [
			'director' => $director
		]);
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param int $id
	 *
	 * return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$request->validate([
			'name' => 'required|max:100',
			'gender' => 'required',
			'born' => [
				'required',
				'regex:/^([0-1]{1})([0-9]{1})\/([0-3]{1})([0-9]{1})\/([0-9]{4})$/',
			],
		]);
		
		$director = Director::find($id);
		if(!$director) throw new ModelNotFoundException;
		
		$this->authorize('manage', $director);
		
		$director->fill($request->all());
		$director->born = date('Y-m-d',strtotime($director->born));
		$director->save();
		
		return redirect()->route('director.index');
	}
}
