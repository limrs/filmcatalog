<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Actor;

class ActorController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	
    /**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$this->authorize('create', Actor::class);
		
		$actor = new Actor();
		
		return view('actors.create', [
			'actor' => $actor,
		]);
	}
	
    /**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->authorize('create', Actor::class);
		
		$request->validate([
			'name' => 'required|max:100',
			'gender' => 'required',
			'born' => [
				'required',
				'regex:/^([0-1]{1})([0-9]{1})\/([0-3]{1})([0-9]{1})\/([0-9]{4})$/',
			],
		]);
		
		$actor = new Actor();
		$actor->fill($request->all());
		$actor->born = date('Y-m-d',strtotime($actor->born));
		if ($actor->save())
		{
			$actor->films()->sync($request->films_id,false);
		}
		
		return redirect()->route('actor.index');
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$this->authorize('view', Actor::class);
		
		$actors = Actor::orderBy('name', 'asc')
		->when($request->query('name'), function($query) use ($request) {
			return $query->where('name', 'like', '%'.$request->query('name').'%');
		})
		->when($request->query('gender'), function($query) use ($request) {
			return $query->where('gender', $request->query('gender'));
		})
		->when($request->query('born'), function($query) use ($request) {
			return $query->where('born', 'like', '%'.date('Y-m-d',strtotime($request->query('born'))).'%');
		})
		->paginate(10);
		
		return view('actors.index', [
			'actors' => $actors
		]);
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$this->authorize('view', Actor::class);
		
		$actor = Actor::find($id);
		if (!$actor) throw new ModelNotFoundException;
		
		return view('actors.show', [
			'actor' => $actor
		]);
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$actor = Actor::find($id);
		if(!$actor) throw new ModelNotFoundException;
		
		$this->authorize('manage', $actor);
		
		return view('actors.edit', [
			'actor' => $actor
		]);
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param int $id
	 *
	 * return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$request->validate([
			'name' => 'required|max:100',
			'gender' => 'required',
			'born' => [
				'required',
				'regex:/^([0-1]{1})([0-9]{1})\/([0-3]{1})([0-9]{1})\/([0-9]{4})$/',
			],
		]);
		
		$actor = Actor::find($id);
		if(!$actor) throw new ModelNotFoundException;
		
		$this->authorize('manage', $actor);
		
		$actor->fill($request->all());
		$actor->born = date('Y-m-d',strtotime($actor->born));
		if ($actor->save())
		{
			$actor->films()->detach();
			if (count($request->films_id) > 0)
			{
				$actor->films()->sync($request->films_id,false);
			}
		}
		
		return redirect()->route('actor.index');
	}
}
