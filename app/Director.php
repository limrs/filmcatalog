<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Director extends Model
{
    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'gender',
		'born',
	];
	
    /**
	 * Get all of the film for the director.
	 */
	public function films()
	{
		return $this->hasMany(Film::class);
	}
}
