<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actor extends Model
{
    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'gender',
		'born',
	];
	
	/**
	 * Get the films belong to the actor.
	 */
	public function films()
	{
		return $this->belongsToMany(Film::class);
	}	
}
