<?php

namespace App\Providers;
use App\Film;
use App\Actor;
use App\Director;
use App\User;
use App\Policies\FilmPolicy;
use App\Policies\ActorPolicy;
use App\Policies\DirectorPolicy;
use App\Policies\UserPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
		Film::class => FilmPolicy::class,
		Actor::class => ActorPolicy::class,
		Director::class => DirectorPolicy::class,
		User::class => UserPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
