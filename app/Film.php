<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'release_date',
		'country',
		'runtime',
		'genre',
		'synopsis',
		'director_id',
	];
	
    /**
	 * Get the director of the film.
	 */
	public function director()
	{
		return $this->belongsTo(Director::class);
	}
	
	/**
	 * Get the actors belong to the film.
	 */
	public function actors()
	{
		return $this->belongsToMany(Actor::class);
	}
}
