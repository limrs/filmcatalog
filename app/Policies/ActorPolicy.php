<?php

namespace App\Policies;

use App\User;
use App\Actor;
use Bouncer;
use Illuminate\Auth\Access\HandlesAuthorization;

class ActorPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
	
	/**
	 * Determine if the given user can view actors
	 *
	 * @param \App\User $user
	 * @return bool
	 */
	public function view(User $user)
	{
		return $user->can('view-actor');
	}
	
	/**
	 * Determine if the given user can create actors
	 *
	 * @param \App\User $user
	 * @return bool
	 */
	public function create(User $user)
	{
		return $user->can('create-actor');
	}
	
	/**
	 * Determine if the given user can manage actors
	 *
	 * @param \App\User $user
	 * @param \App\Actor $actor
	 * @return bool
	 */
	public function manage(User $user, Actor $actor)
	{
		return $user->can('manage-actor');
	}
}
