<?php

namespace App\Policies;

use App\User;
use App\Director;
use Bouncer;
use Illuminate\Auth\Access\HandlesAuthorization;

class DirectorPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
	
	/**
	 * Determine if the given user can view directors
	 *
	 * @param \App\User $user
	 * @return bool
	 */
	public function view(User $user)
	{
		return $user->can('view-director');
	}
	
	/**
	 * Determine if the given user can create directors
	 *
	 * @param \App\User $user
	 * @return bool
	 */
	public function create(User $user)
	{
		return $user->can('create-director');
	}
	
	/**
	 * Determine if the given user can manage directors
	 *
	 * @param \App\User $user
	 * @param \App\Director $director
	 * @return bool
	 */
	public function manage(User $user, Director $director)
	{
		return $user->can('manage-director');
	}
}
