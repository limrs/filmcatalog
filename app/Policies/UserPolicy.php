<?php

namespace App\Policies;

use App\User;
use Bouncer;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
	
	/**
	 * Determine if the given user can view users
	 *
	 * @param \App\User $user
	 * @return bool
	 */
	public function view(User $user)
	{
		return $user->can('view-user');
	}
	
	/**
	 * Determine if the given user can create users
	 *
	 * @param \App\User $user
	 * @return bool
	 */
	public function create(User $user)
	{
		return $user->can('create-user');
	}
	
	/**
	 * Determine if the given user can manage users
	 *
	 * @param \App\User $user
	 * @param \App\User $user
	 * @return bool
	 */
	public function manage(User $user, User $staff)
	{
		return $user->can('manage-user');
	}
}
