<?php

namespace App\Policies;

use App\User;
use App\Film;
use Bouncer;
use Illuminate\Auth\Access\HandlesAuthorization;

class FilmPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
	
	/**
	 * Determine if the given user can view films
	 *
	 * @param \App\User $user
	 * @return bool
	 */
	public function view(User $user)
	{
		return $user->can('view-film');
	}
	
	/**
	 * Determine if the given user can create films
	 *
	 * @param \App\User $user
	 * @return bool
	 */
	public function create(User $user)
	{
		return $user->can('create-film');
	}
	
	/**
	 * Determine if the given user can manage films
	 *
	 * @param \App\User $user
	 * @param \App\Film $film
	 * @return bool
	 */
	public function manage(User $user, Film $film)
	{
		return $user->can('manage-film');
	}
}
